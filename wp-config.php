<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kangdata');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3Ux>O>OB+UFYg_@(vo*Tjs_@)ixiaxNk#)yyYFh6!MeA[GRYnWoPV{^M^&>kBG&a');
define('SECURE_AUTH_KEY',  'VG0*jL;N=4Cu;4k%N=L])hes*y*iw1av]f!p!W~N9Hw&gg#6FX5!)9P@%h[Mgj=P');
define('LOGGED_IN_KEY',    'VV,_GTpR=XY&*Y8EQEyRGO +lhBR-__w4^we[*j9ibY~{Ph|^UF@%Z%ficc3/euQ');
define('NONCE_KEY',        'A<j:cN3gg&oK!7#<2$<tHH(+RgX?9G/z~*BUGAO?lx/Ja,R? f|leeJ*jB|}I{-k');
define('AUTH_SALT',        '7g(0VM}q6? S]7ppD0u|j$V]q%)`x?p;SIA?yK9DW8sFUp;Sn1K8ggkw 4le4htF');
define('SECURE_AUTH_SALT', 'jZ_=>e.r9pr~1-7eS$Q|t`|_vP&nrUtbdJ<,@H^JB{hqG#u~j#7(*f:C*UB}N9hi');
define('LOGGED_IN_SALT',   'la$y57PX4E77 MTjr(Nh/|6uqk*(Yc{H`$_B|Mb|4kr#:vABVf?yeITGZ0I{Jk!m');
define('NONCE_SALT',       '|G*j{r#X8W,eeWIVZM?}r{QsmuH`u{S!vL0O0:3:a,lFwdh~p&Ynuj$4E=@BqpHo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
